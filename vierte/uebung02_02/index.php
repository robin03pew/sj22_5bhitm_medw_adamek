<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>PHP02_02</title>
</head>
<body>

<div class="container d-flex justify-content-center flex-row mt-5">
    <div class="row">
<?php
if(isset($_GET['page'])) {
    if($_GET['page'] == 1){
        echo '<h2>Erste Seite</h2>';
    }
    elseif($_GET['page'] == 2){
        echo '<h2>Zweite Seite</h2>';
    }
    else{
        echo '<h2>Seite ' . $_GET['page'] . '</h2>';
    }
}
if(isset($_GET['id'])) {
    for($x = 1; $x <= $_GET['id']; $x++){
        echo '<div class="col-sm-2 bg-dark text-light border border-info">' . $x . '</div>';
    }
}
?>
        <form method="get" class="d-flex justify-content-center">
            <p>Page: <input type="text" name="page" /></p>
            <p>ID: <input class="pr-3" type="text" name="id" /></p>
            <p><input type="submit" /></p>
        </form>
</div>

</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>