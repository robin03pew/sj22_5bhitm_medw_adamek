<?php
$host = "localhost";
$user = "root";
$password = "insy";
$dbname = "trackstar";
$dsn = "mysql:host=" . $host . ";dbname=" . $dbname;

$pdo = new PDO($dsn, $user, $password);
/* Projekt löschen */
if (isset($_GET['delete'])) {
    $deleteproject_stmt = $pdo->query("DELETE FROM projects WHERE project_id = " . $_GET['delete']);
    $deleteissue_stmt = $pdo->query("DELETE FROM issues WHERE project_id = " . $_GET['delete']);
}
if (isset($_POST['submitIssue'])) {
    if ($_POST['editCategory'] == "ta") {
        $newIssueCategory = "task";
    } elseif ($_POST['editCategory'] == "ft") {
        $newIssueCategory = "feature";
    } elseif ($_POST['editCategory'] == "ti") {
        $newIssueCategory = "ticket";
    }
    $currentDateTime = date('Y-m-d H:i:s');
    $updateIssueStmt = $pdo->prepare('INSERT INTO issues(category, description, created_at, project_id) VALUES(?,?,?,?)');
    $updateIssueStmt->bindParam(1, $newIssueCategory);
    $updateIssueStmt->bindParam(2, $_POST['issuedescr']);
    $updateIssueStmt->bindParam(3, $currentDateTime);
    $updateIssueStmt->bindParam(4, $_GET['showissues']);
    $updateIssueStmt->execute();
}
if (isset($_POST['submitEditProject'])) {
    $currentDateTime = date('Y-m-d H:i:s');
    $updateProjectStmt = $pdo->prepare('UPDATE projects SET title = ?, description=?, created_at=? WHERE project_id = ?');
    $updateProjectStmt->bindParam(1, $_POST['projectName']);
    $updateProjectStmt->bindParam(2, $_POST['projectDescription']);
    $updateProjectStmt->bindParam(3, $currentDateTime);
    $updateProjectStmt->bindParam(4, $_GET['editproject']);
    $updateProjectStmt->execute();
}
?>
<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
            integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
            integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/theme_1642081453985.css">
    <link rel="stylesheet" href="css/styles.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trackstar</title>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Trackstar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        Dropdown
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled">Disabled</a>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>

<!-- Navbar -->
<!-- Projects -->
<div class="container">
    <h1 class="mt-3">Projects</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Date</th>
            <th scope="col">Operations</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";#
        $link = explode("?", $actual_link)[0];
        $projects_stmt = $pdo->query("SELECT * FROM projects");
        while ($row = $projects_stmt->fetch()) {
            echo "<tr>";
            echo "<td>" . $row['title'] . "</td>";
            echo "<td>" . $row['description'] . "</td>";
            echo "<td>" . $row['created_at'] . "</td>";
            echo "<td><a href=" . $link . "?delete=" . $row['project_id'] . "><i class='bi bi-trash'></i></a> <a href=" . $link . "?editproject=" . $row['project_id'] . "><i class='bi bi-pen'></i></a> <a href='index.php?showissues=" . $row['project_id'] . "'><i class='bi bi-info-square'></i></a></td>";
            echo "</tr>";
        }
        echo "</td>";
        echo "</tr>";
        ?>
        </tbody>
    </table>
    <?php
    if (isset($_GET['editproject'])) {
        echo '<h1>Projekt bearbeiten</h1>
<form method="post">
  <div class="form-group">
    <label for="inputName">Name</label>
    <input class="form-control w-25" name="projectName" aria-describedby="projectName" placeholder="Name">
  </div >
  <div class="form-group" >
    <label for="InputDescription">Description</label >
    <input class="form-control mb-2 w-25" name="projectDescription"  id="inputDescription" placeholder = "Description">
  </div>
  <button type="submit" name="submitEditProject" class="btn btn-dark mb-2">Update</button >
</form > ';
    }
    ?>
</div>
<!-- Projects -->
<!-- Issues -->
<?php
if (isset($_GET['showissues'])) {
    $issues_stmt = $pdo->query("SELECT * FROM issues WHERE project_id = " . $_GET['showissues']);
    $issueproject_stmt = $pdo->query("SELECT * FROM projects WHERE project_id = " . $_GET['showissues']);
    while ($row = $issueproject_stmt->fetch()) {
        echo '<div class="container" ><h1> Issues - ' . $row['title'] . ' </h1> ';
    }
    if ($issues_stmt->rowCount() > 0) {
        echo '<table class="table table-striped">
        <thead>
            <tr>
                <th scope = "col" > Kategorie</th>
                <th scope = "col" > Description</th>
            </tr>
        </thead>
        <tbody> ';
        while ($row = $issues_stmt->fetch()) {
            echo '<tr > ';
            echo '<td > ' . $row['category'] . ' </td > ';
            echo '<td > ' . $row['description'] . ' </td > ';
            echo "</tr>";
        }
        echo '</tbody > </table> ';
    } else {
        echo '<h6 > Noch keine Werte vorhanden </h6> ';
    }
    echo '<form method = "post" ><button type = "button" class="btn btn-dark container w-25 mx-auto d-flex justify-content-center mb-4" data-bs-toggle = "modal" data-bs-target = "#exampleModal" >
        Issue hinzufügen
    </button>

<!--Modal -->
<div class="modal fade" id = "exampleModal" tabindex = "-1" aria-labelledby="exampleModalLabel" aria-hidden = "true" >
  <div class="modal-dialog modal-dialog-centered" >
    <div class="modal-content" >
      <div class="modal-header" >
        <h5 class="modal-title" id = "exampleModalLabel" > Issue hinzufügen </h5 >
        <button type = "button" class="btn-close" data-bs-dismiss = "modal" aria-label = "Close" ></button >
      </div >
      <div class="modal-body" >
        <div class="input-group mb-3" >
<select class="form-select col-md-4 mr-3" aria-label = "Default select example" name = "editCategory" >
  <option selected > Kategorie</option >
  <option value = "ft" > Feature</option >
  <option value = "ta" > Task</option >
  <option value = "ti" > Ticket</option >
</select >
<input class="col-md-8" name = "issuedescr" type = "text" class="form-control" placeholder = "Description" aria-label = "Description" aria-describedby = "basic-addon2" >
</div >
      </div >
      <div class="modal-footer" >
        <button type = "button" class="btn btn-secondary" data-bs-dismiss = "modal" > Close</button >
        <button type = "submit" name = "submitIssue" class="btn btn-dark" > Save issue </button >
      </div >
    </div >
  </div >
</div >
</form >
    ';

    echo '</div > ';
}
?>

<!-- Issues -->

<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
    <div class="container p-4">

        <!-- Section: Social media -->
        <section class="mb-4">
            <!-- Facebook -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-facebook"></i
                ></a>

            <!-- Twitter -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-twitter"></i
                ></a>

            <!-- Google -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-google"></i
                ></a>

            <!-- Instagram -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-instagram"></i
                ></a>

            <!-- Github -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-github"></i
                ></a>
        </section>
    </div>
    <!-- Section: Social media -->

    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
        © 2021 Copyright:
        <a class="text-reset fw-bold" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->
</body>
</html>