<?php
session_start();

$_SESSION['is_logged_in'] = null;
$_SESSION['userid'] = null;

session_destroy();
header("Location: login.php");

?>