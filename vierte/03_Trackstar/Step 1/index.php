<?php
$host = "localhost";
$user = "root";
$password = "insy";
$dbname = "trackstar";
$dsn = "mysql:host=" . $host . ";dbname=" . $dbname;

$pdo = new PDO($dsn, $user, $password);
?>
<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/theme_1642081453985.css">
    <link rel="stylesheet" href="css/styles.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trackstar</title>
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Trackstar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        Dropdown
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled">Disabled</a>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
<!-- Navbar -->
<!-- Projects -->
<div class="container">
    <h1>Projects</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Date</th>
            <th scope="col">Operations</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $projects_stmt = $pdo->query("SELECT * FROM projects");
        while ($row = $projects_stmt->fetch()) {
            echo "<tr>";
            echo "<td>" . $row['title'] . "</td>";
            echo "<td>" . $row['description'] . "</td>";
            echo "<td>" . $row['created_at'] . "</td>";
            echo "<td><a href='#'><i class='bi bi-trash'></i></a> <a href='#'><i class='bi bi-pen'></i></a> <a href='#'><i class='bi bi-info-square'></i></a></td>";
            echo "</tr>";
        }
        echo "</td>";
        echo "</tr>";
        ?>
        </tbody>
    </table>
</div>
<!-- Projects -->
<!-- Issues -->

<!-- Issues -->

<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
    <div class="container p-4">

        <!-- Section: Social media -->
        <section class="mb-4">
            <!-- Facebook -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-facebook"></i
                ></a>

            <!-- Twitter -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-twitter"></i
                ></a>

            <!-- Google -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-google"></i
                ></a>

            <!-- Instagram -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-instagram"></i
                ></a>

            <!-- Github -->
            <a class="btn btn-outline-dark btn-floating m-1" href="#!" role="button"
            ><i class="bi bi-github"></i
                ></a>
        </section>
    </div>
    <!-- Section: Social media -->

    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
        © 2021 Copyright:
        <a class="text-reset fw-bold" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->
</body>
</html>