<?php
$host = "localhost";
$user = "root";
$password = "insy";
$dbname = "trackstar";
$dsn = "mysql:host=" . $host . ";dbname=" . $dbname;
if(!$_SESSION['is_logged_in']){
    header("Location: login.php");
}

$id = $_POST['id'];

$pdo = new PDO($dsn, $user, $password);

$deleteProject = "DELETE FROM projects WHERE project_id=" . $id . ";";
$deleteIssues = "DELETE FROM issues WHERE project_id=" . $id . ";";

$delStmt = pdo->prepare($deleteProject);
$delStmt2 = pdo->prepare($deleteIssues);
$delStmt->execute();
$delStmt2->execute();
?>