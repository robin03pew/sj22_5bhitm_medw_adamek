<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        body{
            background-color: #2C3034;
        }
    </style>
</head>

<body>
<table class="table table-dark table-striped w-50 mx-auto">
    <thead>
    <tr>
        <th scope="col">Key</th>
        <th scope="col">Value</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $counter = 0;
    foreach ($_SERVER as $key => $value) {
        if($counter >4){
            return;
        }
        echo '<tr>
<td>' . $key . '</td>
<td>' . $value . '</td></tr>';
        $counter++;
    }
    ?>
    </tbody>
</table>

<?php
phpinfo();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
        crossorigin="anonymous"></script>
</body>
</html>