<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Übung_02_03</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 class="text-center">Alle Datensätze</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Name</th>
            <th scope="col">Geschlecht</th>
            <th scope="col">Email</th>
            <th scope="col">Straße</th>
            <th scope="col">Stadt</th>
            <th scope="col">Postleitzahl</th>
            <th scope="col">Bundesland</th>
            <th scope="col">Interessensgebiete</th>
        </tr>
        </thead>
        <tbody>
        <?php


        $host = "localhost";
        $user = "root";
        $password = "insy";
        $dbname = "contactform";

        $dsn = "mysql:host=". $host . ";dbname=". $dbname;

        $pdo = new PDO($dsn,$user,$password);
        $stmt = $pdo->query("SELECT * FROM users");
        while ($row = $stmt->fetch()) {
            echo "<tr>";
            echo "<th scope='row'>".$row['USER_ID']."</th>";
            echo "<td>".$row['TITLE']."</td>";
            echo "<td>".$row['FIRST_NAME']." ".$row['LAST_NAME']."</td>";
            echo "<td>".$row['GENDER']."</td>";
            echo "<td>".$row['EMAIL']."</td>";
            echo "<td>".$row['STREET']."</td>";
            echo "<td>".$row['CITY']."</td>";
            echo "<td>".$row['POSTCODE']."</td>";
            echo "<td>".$row['STATE']."</td>";
            echo "<td>".$row['INTEREST']."</td>";
            echo "<td> <a href=db.php?id=" . $row['USER_ID']. "> </td>";
        }

        /*$stmt = $pdo->query("SELECT * FROM USER_HAS_FIELDS_JT WHERE USER_ID = 2");
        echo "<td>";
        while ($row = $stmt->fetch()) {
            echo $row['FIELD']."\n";
        }*/
        echo "</td>";
        echo "</tr>";
        ?>
        </tbody>
    </table>
</div>
</body>