<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
session_start();
$_SESSION['foo'] = 'bar';
$value = 'foo';
echo session_id(); // Anzeige der Session-ID
print_r($_SESSION);
echo "<br>";
setcookie("TestCookie", $value);
echo "<br>";
setcookie("TestCookie", $value, time()+3600); /* verfällt in 1 Stunde */
setcookie("TestCookie", $value, time()+3600, "/foo/", "example.com", 1);
echo "<br>";

// ein bestimmtes Cookie ausgeben
echo $_COOKIE["TestCookie"];
echo "<br>";
// Ein anderer Weg zu Debuggen/Testen ist, alle Cookies anzuzeigen
print_r($_COOKIE);
?>
</body>
</html>