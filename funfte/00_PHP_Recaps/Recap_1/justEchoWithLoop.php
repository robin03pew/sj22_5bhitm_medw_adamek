<?php
function echoarray($array)
{
    foreach ($array as $value) {
        ?>
        <div class="card mt-2 mx-auto w-100" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title"><?php print_r($value["title"]) ?> </h5>
                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                <p class="card-text"><?php print_r($value["description"]) ?></p>
            </div>
        </div>
        <?php
    }
}
?>

