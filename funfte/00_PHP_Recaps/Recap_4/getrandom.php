<?php
$students = [[0, "Martin Bauer", "True"],
    [1, "Robin Adamek", "True"],
    [2, "Nico Fichtinger", "True"],
    [3, "Armin Siegmeth", "False"],
    [4, "Oliver Fahrnik", "True"],
    [5, "Florian Marek", "True"],
    [6, "Alexander Kern", "True"],
    [7, "Julian Pronhagl", "True"],
    [8, "Emanuel Ardelean", "True"],
    [9, "Julia Schenk", "True"],
    [10, "Johanna Bock", "True"],
    [11, "Johannes Spindl", "True"],
    [12, "Dome Ferfecky", "True"],
    [13, "Simon Engelberger", "True"],
    [14, "Dennis Strupp", "True"],
    [15, "David Kaufmann", "True"],
    [16, "Gabriel Zeller", "True"],
    [17, "Fabian Maierhofer", "True"],
    [18, "Daniel Hochegger", "True"],
    [19, "Paul Permoser", "True"],
    [20, "Sebastian Huber", "True"],
    [21, "Alex Dick", "True"]];


$nstundents = [];
foreach ($students as $student){
    if($student[2] == "True"){
        $nstundents[] = $student;
    }
}

if(empty($nstundents)){
    echo "Keine verfügbaren Schüler/innen vorhanden";
}else{
    $value = $nstundents[array_rand($nstundents)];
    echo $value[1];
}
?>
