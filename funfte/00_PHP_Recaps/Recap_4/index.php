<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://csshake.surge.sh/csshake.min.css">
    <title>Hello, world!</title>
</head>
<body>
<?php
$students = [[0, "Martin Bauer", "True"],
    [1, "Robin Adamek", "True"],
    [2, "Nico Fichtinger", "True"],
    [3, "Armin Siegmeth", "False"],
    [4, "Oliver Fahrnik", "True"],
    [5, "Florian Marek", "True"],
    [6, "Alexander Kern", "True"],
    [7, "Julian Pronhagl", "True"],
    [8, "Emanuel Ardelean", "True"],
    [9, "Julia Schenk", "True"],
    [10, "Johanna Bock", "True"],
    [11, "Johannes Spindl", "True"],
    [12, "Dome Ferfecky", "True"],
    [13, "Simon Engelberger", "True"],
    [14, "Dennis Strupp", "True"],
    [15, "David Kaufmann", "True"],
    [16, "Gabriel Zeller", "True"],
    [17, "Fabian Maierhofer", "True"],
    [18, "Daniel Hochegger", "True"],
    [19, "Paul Permoser", "True"],
    [20, "Sebastian Huber", "True"],
    [21, "Alex Dick", "True"]];

?>

<div class="container mt-3">
    <div id="pop-up" style="position:absolute; display: none; left:50%; top:50%; transform: translate(-50%, -50%); z-index: 1;
      width: 600px; background-color: white; border: 2px solid black; padding: 10px">
        <h5>Randomizer</h5>
        <p class="text-center" style="font-size: 25px">Schülerin/Schüler:</p>
        <p class="text-center" style="font-size: 30px; color: red;" id="student">adasdd</p>
        <button type="button" id="close-popup" style='display: block; margin: auto;' class="btn btn-warning">Schließen</button>
    </div>
    <h1>5BHITM</h1>
    <button id="start" type="button" class="btn btn-danger" style='display: block; margin: auto;font-size: 30px; width: 350px;'>Let’s go >>></button>
    <div class="row">
        <?php
        foreach ($students as $value) {
        ?>
        <div class="col-2 mt-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center"><?php echo "Student ".$value[0]?></h5>
                    <p class="card-text text-center"><?php echo "Student ".$value[1]?></p>
                    <?php
                        if($value[2] == "True"){
                            echo "<p class='card-text text-center btn btn-success' style='display: block; margin: auto;'>Verfügbar</p>";
                        }else{
                            echo "<p class='card-text text-center btn btn-danger' style='display: block; margin: auto;'>Nicht Verfügbar</p>";
                        }
                        ?>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $("#start").click(function () {
            $("#pop-up").hide();
            $(".card").addClass("shake-little").addClass("shake-constant");
            setTimeout(function(){
                $(".card").removeClass("shake-little").removeClass("shake-constant");
                $("#pop-up").show();
                $.ajax(
                    {
                        url: 'getrandom.php',
                        type: 'GET',
                        success: function(data)
                        {
                            $("#student").text(data);
                        }
                    })
            }, 3000);
        })
        $("#close-popup").click(function () {
            $("#pop-up").hide();
        })
    });
</script>
</body>
</html>