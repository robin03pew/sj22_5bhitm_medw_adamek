<?php
if(isset($_POST['submitbutton'])){
    $kwh = $_POST['kwh'];
}
?>
<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
    <title>Recap 3</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Project Name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <h1>Strompreisvergleich</h1>
    <form method="post">
    <label class="form-label">kwH/Jahr</label>
    <input type="number" name="kwh" class="form-control w-25">
    <button type="submit" name="submitbutton"  class="btn btn-outline-secondary mt-2">Jetzt vergleichen!</button>
    </form>
    <?php
    $wasserkraft = $energienoe = $maxienergy =0;
    if(isset($_POST['submitbutton'])){
        switch ($kwh){
            case $kwh<=1000:
                $wasserkraft = $kwh*10.23;
                $energienoe = $kwh*10.4;
                $maxienergy = $kwh*10.49;
                echo 'kwh unter 1000';
                break;
            case $kwh>1000&&$kwh<=3000:
                $wasserkraft = $kwh*9.9;
                $energienoe = $kwh*10.2;
                $maxienergy = $kwh*9.9;
                echo 'kwh zw 1001 und 3000';
                break;
            case $kwh>3000:
                $wasserkraft = $kwh*9.35;
                $energienoe = $kwh*7.9;
                $maxienergy = $kwh*9.45;
                echo 'über 3000';
                break;
            default:
                echo 'default du oasch';
        }

    ?>
    <h1 class="mt-3">3 Angebote</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Anbieter</td>
                    <td>Wichtige Informationen</td>
                    <td>Jahrespreis</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Unsere Wasserkraft</td>
                    <td>Tarif "Aquastrom Basic"</td>
                    <td>
                        <?php
                            echo "€ ".number_format($wasserkraft, 2, ',', '.')
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Energie NÖ</td>
                    <td>Tarif "Strom Privat"</td>
                    <td>
                        <?php
                            echo "€ ".number_format($energienoe, 2, ',', '.')
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Maxi Energie</td>
                    <td>Tarif "MAX Basic Strom</td>
                    <td>
                        <?php
                            echo "€ ".number_format($maxienergy, 2, ',', '.')
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php
    }
    ?>
</div>
</body>
</html>