<?php
/**
* Plugin Name: currentdate
* Description: gibt derzeitiges Datum aus
* Version: 0.1
* Author: HTL Kranker Chef
*/

add_filter('the_content', 'addDateToEndOfPost');
function addDateToEndOfPost($content){
    return $content . "<p style='text-align:right; padding:5px; background:rgba(0,0,0,0.3); color:#ccc'>".date('d.m.Y')."</p>";
} ?>