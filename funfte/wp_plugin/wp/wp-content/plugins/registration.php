<?php
/**
 * Plugin Name: Registration 
 * Description: Ein Registrierungs-plugin
 * Version: 1.0
 * Author: Robin Adamek
 */



function registration_form( $email, $first_name, $last_name ) {
  function wpse_load_plugin_css() {
    wp_enqueue_style( 'style', plugins_url( 'style.css' , __FILE__ ));
  }
   echo '
   <style>
   div {
       margin-bottom:2px;
   }
    
   input{
       margin-bottom:4px;
   }
   </style>
   ';

   echo '
   <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">

   <div>
   <label for="firstname">First Name</label>
   <input type="text" name="fname" value="' . ( isset( $_POST['fname']) ? $first_name : null ) . '">
   </div>
    
   <div>
   <label for="website">Last Name</label>
   <input type="text" name="lname" value="' . ( isset( $_POST['lname']) ? $last_name : null ) . '">
   </div>

   <div>
   <label for="email">Email <strong>*</strong></label>
   <input type="text" name="email" value="' . ( isset( $_POST['email']) ? $email : null ) . '">
   </div>
   <div>
   <label for="newsletter">Newsletter</label>
   <input type="checkbox" id="newsletter" name="newsletter" value="check" checked">
 </div>
    
   <input type="submit" class="btn" name="submit" value="Register"/>
   </form>
   ';
}
function registration_validation( $email, $first_name, $last_name)  {
   global $reg_errors;  //globale var
   $reg_errors = new WP_Error; //neue instanz von WP_Error
        
   if ( !is_email( $email ) ) {
       $reg_errors->add( 'email_invalid', 'Email is not valid' );
   }
    
   if ( email_exists( $email ) ) {
       $reg_errors->add( 'email', 'Email Already in use' );
   }
        
   if ( is_wp_error( $reg_errors ) ) {
        
       foreach ( $reg_errors->get_error_messages() as $error ) {
            
           echo '
           <div>
           <strong>ERROR</strong>:';
           echo $error . '<br/>';
           echo '</div>';
            
       }
        
   }
}
function complete_registration() {
  global $wpdb;
  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE `{$wpdb->base_prefix}registered_users` (
    user_email varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    created_at datetime NOT NULL,
    newsletter bool not null,
    PRIMARY KEY  (user_email)
  ) $charset_collate;";

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($sql);

  // Check if the checkbox is checked
  if (isset($_POST['newsletter']) && $_POST['newsletter'] == 'check') {
    $news = 1;
  } else {
    $news = 0;
  }

  global $reg_errors, $email, $first_name, $last_name;
   
  if ( 1 > count( $reg_errors->get_error_messages() ) ) {
    $wpdb->insert("wp_registered_users", [
      'user_email' => $email,
      'first_name' => $first_name,
      'last_name' => $last_name,
      'created_at' => current_time('mysql'),
      'newsletter' => $news
    ]);
    echo 'Danke fürs Registrieren. Seas ' . $first_name . ' ' . $last_name;
  }
 
       $ding = $wpdb->get_results("SELECT * FROM wp_registered_users ");
       var_dump($ding);
   }


function custom_registration_function() {
   if ( isset($_POST['submit'] ) ) {
       registration_validation(
       $_POST['email'],
       $_POST['fname'],
       $_POST['lname'],
       );
        
       // sanitize von form user input
       global  $email, $first_name, $last_name;
       $email      =   sanitize_email( $_POST['email'] );
       $first_name =   sanitize_text_field( $_POST['fname'] );
       $last_name  =   sanitize_text_field( $_POST['lname'] );


       // call von complete_registration um den user zu erstellen
       // nur wenn WP_ERROR nicht gefunden wird
       complete_registration(
       $email,
       $first_name,
       $last_name,
       );
   }

   registration_form(
       $email,
       $first_name,
       $last_name
       );
}

//Neuen shortcode registrieren: [cr_custom_registration]
add_shortcode( 'cr_custom_registration', 'custom_registration_shortcode' );
 
// Die callback function die [book] ersetzen wird
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}